(ns interactive-cv.ashaw.view
  (:require [clojure.spec.alpha :as s]
            [interactive-cv.pdf :refer [pdf-canvas]]
            [clojure.set :as set]
            [clojure.edn :as edn]
            [clojure.string :as str]
            [interactive-cv.ashaw.settings :refer [settings state] :as settings]
            [json-html.core :refer [edn->hiccup]]))

(defn page [{:keys [placeholder debug] :as props
                  :or   {debug (:debug @settings)}}
                 & [section :as args]]
  (if-not (map? props)
    (into [page {}] args)
    [:div {:style {:display          "flex"
                   :justify-content  "center"
                   :align-items      "center"
                   :min-height       "100vh"
                   :font-family      "Garamond, Georgia, serif"
                   :opacity          (or (:opacity @state) 1)
                   :background-color (:background-color @settings)
                   :transition       (str "background-color " (:transition-time @settings)
                                          ", "
                                          "opacity " (:transition-time @settings))}}
     [:div {:style {:width            "800px"
                    :margin-top       "8rem"
                    :margin-bottom    "8rem"
                    :min-height       "60vh"
                    :box-shadow       (str (:shadow-x @settings) "rem "
                                           (:shadow-y @settings) "rem "
                                           "0.2rem rgba(0,0,0,0.5)")
                    :max-height       "60%"
                    :background-color (:foreground-color @settings)
                    :transition       (str "background-color " (:transition-time @settings)
                                           ", "
                                           "box-shadow "(:transition-time @settings))
                    :display          "flex"
                    :padding          "8% 4%"
                    :flex-direction   "column"}}
      [view/item section]]]))

(defn color-words [text & colors]
  (let [words       (re-seq #"\w* *" text)
        colors      (-> @settings (select-keys [:palette-A :palette-B :palette-C]) vals)
        color-words (partition 2 (interleave colors words))]
    (into [:div {:style
                 {:font-family
                  (:heading-font @settings)
                  :font-style "italic"}}]
          (for [[color word] color-words]
            [:span {:style {:color color}} word]))))

(defn landing-links [{[{cells :children}] :children :as links}]
  (into [:div]
        (for [{contents :children} cells
              :let                 [{:keys [description value]} contents]]
          [edn->hiccup description value])))

(defmethod view/item ::view/COLORWORDS
  [{[{[_ _ {title :value}] :children} & children] :children}]
  [:div
   [:h1 {:style {:margin "unset" :font-weight "normal"}}
    [color-words title]]
   (into [:<>] (for [child
                     (filter (comp not tagged?) children)]
                 [view/item child]))])

(defmethod view/item :hr [_]
  [:hr {:style {:border-top    (str "1px solid " (:palette-C @settings))
                :border-bottom "none" :border-left "none" :border-right "none"
                :width         "100%" :margin      "30px 0 18px 0"}}])

(defmethod view/item ::view/BOOK [{[_
                                {[subtitle] :children}
                                {[{cover :value}] :children}
                                & descriptions] :children}]
  [:div {:style {:display         "flex" :width "100%"
                 :justify-content "center"
                 :flex-wrap       "wrap"
                 :flex-direction  "row"
                 :margin-bottom   "4rem"}}
   [:div {:style {:max-width "90%"
                  :display   "flex"}}
    [:img {:src cover :style {:height     "20rem"
                              :max-width  "30%"
                              :object-fit "contain"}}]
    [:div {:style {:flex        1
                   :margin-left "2rem"}}
     [:h2 [view/item subtitle]]
     [:div {:style {:display         "flex"
                    :justify-content "center"
                    :width           "100%"}}
      (into [:div {:style {:flex        1
                           :margin-left "2rem"}}]
            (map #(vector view/item %))
            descriptions)]]]])


(defmethod view/item ::view/MEDIUM
  [{[{title :content}
     {[image] :children}
     & descriptions] :children :as data}]
  [:div {:style {:min-width       "100%"
                 :display         "flex"
                 :justify-content "space-between"
                 :margin-bottom   "6rem"}}
   [:div {:style {:max-width  "40%"
                  :min-width  "40%"
                  :max-height "20rem"}}
    [view/item image]]
   [:div {:style {:text-align  "center"
                  :margin-left "6rem" }}
    [:h2 {:style {:font-family (:heading-font @settings)}} title]
    [:div {:style {:display         "flex"
                   :flex            1
                   :justify-content "center"
                   :min-width       "18rem"}}
     (into [:div]
           (comp (filter (complement tagged?))
                 (map #(vector view/item %)))
           descriptions)]]])

(defmethod view/item ::view/LINKLIST
  [{[_ {[image-row link-row] :children} & children] :children}]
  (into [:div
         [view/item image-row]
         [view/item link-row]]
        (comp (filter (complement tagged?))
              (map #(vector view/item %)))
        children))

(defmethod view/item ::view/MARGIN [{:keys [children]}]
  (into [:div {:style {:margin "2rem"}}]
        (map #(vector view/item %))
        children))

(defmethod view/item :section [{:keys [children level]}]
  (into [:div.section]
        (for [child
              (filter (comp not tagged?) children)]
          [view/item child])))

(defmethod view/item :default [{:keys [children] :as data}]
    (into [:div.section (when (:debug @settings) [edn->hiccup data])]
          (for [child
                (filter #(not (s/valid? :interactive-cv.spec/section %)) children)]
          [view/item child])))

(defmethod view/item :section.placeholder [{:keys [children level]}]
  (into [:div.section]
        (for [child (filter #(not (and (contains? #{:view/item :link}
                                                  (keyword (:type %)))
                                       (s/valid? :interactive-cv.spec/section %)))
                            children)]
          [view/item child])))

(defmethod view/item :headline [{:keys [level content]}]
  [(keyword (str "h" level)) {:style {:font-family (:heading-font @settings)}} content])

(defmethod view/item :paragraph [{:keys [children]}]
  (into [:div.block-paragraph]
        (for [child children]
          [view/item child])))

(defmethod view/item :text.plain [{text :value}]
  (when (seq text) [:span text]))

(defmethod view/item :text.italic [{text :value}]
  [:i text])

(defmethod view/item :text.bold [{text :value}]
  [:strong text])

(defmulti org-block
  (fn [{:keys [name]}] (keyword name)))

(defmethod view/item :block [data]
  [org-block data])

(defn anchor [{:keys [description value]}]
  [:a {:href  value
       :style {:color      (:link-color @settings)
               :transition (str "color " (:transition-time @settings))}}
   description])

(defmethod view/link :default [m] [anchor m])

(defmethod view/internal-link :default [m] [anchor m])

(defn image-link [{:keys [value]}]
  [:img {:src value
         :width "100%"
         :height "100%"
         :object-fit "contain"}])

(for [v [:png :jpg :jpeg :gif]]
  (defmethod view/internal-link v [m] [image-link m]))

(defmethod view/item :content [args]
  (let [original (second args)
        parsed   (try (edn/read-string (str "[" original "]"))
                      (catch js/Error e (do (println (type e)) original)))]
    (into [:div] (for [section parsed]
                   [view/item section]))))

(defmethod view/item :list [{:keys [_ _ children]}]
  (into [:ul] (for [child children]
                [view/item child])))

(defmethod view/item :list.item [{:keys [_ children tag]}]
  (let [options  (filter #(str/includes? (:type %) "list.item") children)
        children (seq (set/difference (set children) (set options)))]
    (into [:li {:style {:text-align    "left"
                        :margin-bottom "1rem"}} (when tag [:i tag]) (when tag [:br])]
          (for [child children]
            [view/item child]))))

(defmethod view/item :table.cell [{:keys [children]}]
  (into [:div {:style {:min-height "100%"
                       :flex       1
                       :text-align "center"}}]
        (for [child children]
          [view/item child])))

(defmethod view/item :table.row [{:keys [children]}]
  (into [:div {:style {:display         "flex"
                       :justify-content "space-between"
                       :width           "100%"
                       :height          "20%"}}]
        (for [child children]
          [view/item child])))

(defmethod org-block :embed [{name :name [subtype & params] :params}]
  (let [params (zipmap
                (map edn/read-string (take-nth 2 params))
                (take-nth 2 (rest params)))]
    [:iframe (into {:loading "lazy"
                    :src     (:url params)} params)]))
