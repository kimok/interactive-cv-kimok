(ns interactive-cv.ashaw.settings
  (:require
   [clojure.string :as str]
   [interactive-cv.core :as cv]
   [reagent.core :as r]
   [reagent.ratom :refer [reaction]]
   [clojure.walk :refer [keywordize-keys]]))

(defn drawer->map [drawer]
  (when drawer
    (->> drawer
         (re-seq #":(.*): (.*)(\n|$)" )
         (map (comp rest butlast))
         flatten
         (apply hash-map)
         keywordize-keys)))

(defn orga-params->map [orga-list]
  (when orga-list
    (->> orga-list (map #(str/replace % #":" "")) (apply hash-map) keywordize-keys)))

(defn get-drawer [{:keys [children]}]
  (first (filter #(= "drawer" (:type %)) children)))

(def defaults
  {:heading-font "serif"
   :transition-time "0s"
   :shadow-x 1
   :shadow-y 1})

(def settings
  (reaction (let [data @cv/content
                  global-drawer (-> data :children first :children second :value)
                  current-drawer (:value (get-drawer @section/current))]
              
              (merge
               defaults
               (drawer->map global-drawer)
               (drawer->map current-drawer)))))

(defonce state (r/atom {:opacity 0}))

(add-watch settings :watcher
  (fn [key atom old-state new-state]
    (prn "-- Atom Changed --")
    (prn "key" key)
    (prn "atom" atom)
    (prn "old-state" old-state)
    (prn "new-state" new-state)))
