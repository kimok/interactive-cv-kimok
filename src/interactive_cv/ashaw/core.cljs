(ns interactive-cv.ashaw.core
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [<! timeout]]
            [interactive-cv.ashaw.view :as view]
            [interactive-cv.ashaw.settings :refer [settings state]]
            [interactive-cv.core :as cv]
            [json-html.core :refer [edn->hiccup]]
            [shadow.resource]))

(shadow.resource/inline "content/ashaw/content.org")

(defonce fade-in (go (<! (timeout 500))
                     (reset! state {:opacity 1})))

(defn root-component []
  [:div
   [view/orga-page {:placeholder nil} @section/current]])

(defn init []
  (cv/fetch-content
   {:on-response #(set! js/document.title (:title @settings))})
  (cv/mount root-component))
