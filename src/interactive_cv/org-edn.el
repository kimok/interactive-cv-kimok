(require 'project)
(let ((lib (concat (project-root (project-current)) "lib/")))
  (add-to-list 'load-path (concat lib "parseclj/"))
  (load (concat lib "parseedn/parseedn.el")))
(require 'parseedn)

(defun list->vec (x)
  (if (not (sequencep x)) x
    (vconcat [] x)))

(defun org->edn (output-filename)
  (org-mode)
  (setq org-link-types-re "\\`\\([a-z]+\\):"
        org-use-sub-superscripts '{})
  (let* ((tree (org-element-parse-buffer)))
    (org-element-map tree (append org-element-all-elements
                                  org-element-all-objects '(plain-text))
      (lambda (x)
        (if (org-element-property :parent x)
            (org-element-put-property x :parent nil))
        (if (org-element-property :structure x)
            (org-element-put-property x :structure nil))
        (if (and (org-element-property :tag x)
                 (listp (org-element-property :tag x)))
            (org-element-put-property
             x :tag
             (list->vec
              (org-element-property :tag x))))))
    (with-temp-file output-filename
      (insert (parseedn-print-str tree)))))

