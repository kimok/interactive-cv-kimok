(ns interactive-cv.zip
  (:require [clojure.zip :as z]))

(defn top [loc]
  (->> loc
       (iterate z/up)
       (take-while some?)
       last))

(defn walk [loc f & args]
  (loop [loc loc]
    (if (z/end? loc)
      (z/root loc)
      (recur (z/next (apply f loc args))))))

(defn root? [z]
  (nil? (z/up z)))

(defn seek [loc pred]
  (->> (iterate z/next loc)
       (take-while (comp not z/end?))
       (reduce #(when (pred (z/node %2))
                  (reduced %2))
               loc)))

(defn child-locs [loc]
  (->> (z/down loc)
       (iterate z/right)
       (take-while some?)))
