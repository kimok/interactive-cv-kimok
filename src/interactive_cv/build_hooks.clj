(ns interactive-cv.build-hooks
  (:require
   [clojure.edn :as edn]
   [clojure.java.shell :refer (sh)]
   [clojure.zip :as z]
   [interactive-cv.org-edn :as org]
   [interactive-cv.parse :as parse]
   [interactive-cv.util :as u]))

(defn org->edn
  {:shadow.build/stage :compile-prepare}
  [build-state build]
  (sh "emacs" "-batch" "-l"
      "src/interactive_cv/org-edn.el"
      (str "src/content/" (name build) "/content.org")
      "--eval" "(org->edn \"content.edn\")")
  (let [edn-file (str "src/content/" (name build) "/content.edn")
        new-edn (-> (slurp edn-file)
                    clojure.edn/read-string
                    u/vectorize
                    (update 1 (fnil identity {}))
                    org/->zipper
                    (org/zip-walk parse/infer-node)
                    (org/zip-walk parse/insert-post-blank)
                    (org/zip-walk z/edit parse/infer-type)
                    (org/zip-walk z/edit parse/element->component)
                    (org/zip-walk z/edit parse/infer-subtype)
                    z/root)]
    (spit edn-file new-edn)
    (sh "rsync" "-a"
        (str "src/content/" (name build) "/")
        "public/")
    build-state))
