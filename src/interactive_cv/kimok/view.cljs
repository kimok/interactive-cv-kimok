(ns interactive-cv.kimok.view
  (:require [interactive-cv.core :as cv]
            [interactive-cv.util :as u]
            [interactive-cv.defer :as d]
            [clojure.zip :as z]
            [interactive-cv.route :as route :refer [route]]
            [interactive-cv.org-edn :as org]
            [reagent.core :as r]
            [reagent.ratom :refer [reaction]]
            [garden.core :refer [css]]
            ["react-transition-group" :refer [CSSTransition TransitionGroup]]))

(defn text-bubble
  "bubble-shaped text box, absolutely centered"
  [& [{:keys [style on-click speed]
       :or {speed 0.5}}
      & content]]
  [:div.text-bubble
   {:on-click on-click
    :pointer-events "none"
    :style (-> {:position "absolute"
                :display "flex"
                :background "white"
                :justify-content "center"
                :align-items "center"
                :border-radius "50%"
                :border "5px solid black"
                :box-sizing "border-box"
                :padding "0.5rem"
                :cursor "default"
                :user-select "none"
                :font-weight "bold"
                :transform "translate(-50%, -50%)"
                :transition (str "all " speed "s ")
                :word-break "break-word"
                :text-align "center"}
               (merge style))}
   content])

(defn orbiter
  "translates, then rotates an element.
  rotates the element back at the resulting position, so it stays level.
  radius in pixels as an int, or any css unit as a string.
  angle from 0 to 1 (float)"
  [& [{radius :radius angle :angle
       {:keys [transition z-index]} :style
       :or {radius 100 angle 0}}
      & content]]
  [:div {:style {:position "relative"
                 :z-index z-index}}
   [:div.orbit
    {:style {:position "absolute"
             :width radius
             :height 0
             :margin-left "50%"
             :margin-top "50%"
             :transform-origin "0% 0%"
             :transition transition
             :transform (str "Rotate(" (* angle 360)
                             "deg)")}}
    (into [:div
           {:style {:margin-left "100%"
                    :transition transition
                    :transform
                    (str "Rotate(" (- (* angle 360)) "deg)")}}]
          content)]])

(defn bubble-router
  [& [{:keys [node]}]]
  (let
   [last-angle (r/atom (rand))
    last-node  (r/atom node)]
    (fn [& [{:keys [ratio speed on-navigate on-restart clickable? node edges parent]
             :or {ratio 1.5 speed 0.75}
             {:keys [width height]
              :or   {width 200 height 200}
              :as   style} :style}]]
      (let [padded-width (/ width ratio)
            big          (/ padded-width ratio)
            small        (/ big ratio)
            radius       (/ big ratio)
            outer-radius (int (+ (/ small 2) (u/hypotenuse (/ width 2) (/ height 2))))]
        [:div {:style style}
         [:style
          (css [:.edge-enter :.edge-exit-active :.node-exit-active :.node-enter
                [:.orbit {:width   (str outer-radius "px !important")
                          :z-index 0}]]
               [:.edge-enter-active [:.orbit {:width (str radius "px !important")}]]
               [:.node-enter-active [:.orbit {:width "0 !important"}]])]
         (into [:> TransitionGroup]
               (for [id   (-> edges (into [node]) set sort)
                     :let [node? (= node id)
                           size  (if node? big small)
                           index (u/nth-of edges @last-node)
                           angle (-> (when ((set edges) id)
                                       (/ (u/nth-of (u/shift index edges) id)
                                          (count edges)))
                                     (+ @last-angle) (+ 0.5) (rem 1))]]
                 [:> CSSTransition {:classNames (if node? "node" "edge")
                                    :timeout    (* speed 1000)
                                    :in         true
                                    :key        id}
                  [orbiter
                   {:radius (if-not node? radius 0)
                    :angle  angle
                    :style  {:z-index (condp contains? id #{parent last-node} 0 #{node} 1 2)
                             :transition (str "width " speed "s , height " speed "s"
                                              (when node?
                                                (str ", transform " speed "s step-end")))}}
                   [text-bubble {:speed    speed
                                 :style    {:width size :height size}
                                 :on-click #(when (and (not node?) clickable?)
                                              (when on-navigate (on-navigate id))
                                              (reset! last-angle angle)
                                              (reset! last-node node))}
                    id]]]))
         [:div {:on-click #(when (and on-restart clickable?)
                             (on-restart))
                :style    {:height "100%" :width "100%"}}]]))))

(defn sidebar []
  (let [edges (reaction (->> (sequence @cv/child-routes)
                             (cons @cv/parent-route)
                             (keep org/route)
                             (map route/slug)))]
    (fn []
      [:div {:style {:min-width     300
                     :padding-right 20
                     :height        "100vh"
                     :overflow      "hidden"}}
       [bubble-router {:style
                       {:width    300
                        :height   300
                        :overflow "hidden"}
                       :parent (org/tag @cv/parent-route)
                       :node @route
                       :edges @edges
                       :clickable? (not @d/defer?)
                       :on-restart #(do (route/go! @cv/root-route) (d/defer! 1000))
                       :on-navigate #(do (route/go! %) (d/defer! 675))}]
       [:img {:src "kimo-suit.jpg" :style {:width 300}}]])))

(defn page []
  [:div {:style {:overflow-y "auto" :flex-grow 5
                 :overflow-x "hidden"}}
   [:div {:style {:margin-right "10%"}}
    (some->> @cv/zipper z/node (conj [org/hiccup]))]])

(defn contact []
  (let [{:keys [author email]} @cv/org-header]
    [:div {:style {:position    "fixed" :bottom 0 :color 'white
                   :text-shadow "1px 1px 1px black"}}
     author [:br]
     [:a {:href (str "mailto:" email) :style {:color 'white}} email]]))
