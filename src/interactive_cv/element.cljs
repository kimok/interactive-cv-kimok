(ns interactive-cv.element
  (:require [interactive-cv.org-edn :as org
             :refer [element hiccup prune]]
            [interactive-cv.util :as u]
            [interactive-cv.core :as cv]
            [interactive-cv.defer :as defer]
            [interactive-cv.pdf :as pdf]
            [gnu.org-mode :as-alias org-mode]
            [gnu.org-mode.link :as-alias link]
            [gnu.org-mode.link.file :as-alias file])
  #_(:require-macros [interactive-cv.element :refer [defel]]))

(defmethod element ::org-mode/text [node]
  [:span (org/prop :value node)])

(defmethod element ::org-mode/section [_] [:div])

(defmethod element ::org-mode/paragraph [_] [:p])

(defmethod element ::org-mode/italic [_] [:i])

(defmethod element ::org-mode/bold [_] [:strong])

(defmethod element ::org-mode/underline [_] [:u])

(derive ::org-mode/verbatim ::org-mode/code)

(defmethod element ::org-mode/code [[_ {:keys [value]}]]
  [:code value])

(defmethod element ::org-mode/strike-through [[_ {:keys [value]}]]
  [:span {:style {:text-decoration "line-through"}} value])

(defmethod element ::org-mode/keyword [_] [:<>])

(defmethod element ::org-mode/timestamp.active
  [[_ {:keys [year-start month-start day-start year-end month-end day-end]}]]
  [:i
   (str year-start "." month-start "." day-start)
   (when (not= [year-start month-start day-start] [year-end month-end day-end])
     (str year-end "." month-end "." day-end "."))])

(defmethod element ::org-mode/subscript [_] [:<>])

(defmethod element ::org-mode/horizontal-rule [_] [:hr])

(defmethod element ::org-mode/headline [_] [:div])

(defmethod element ::org-mode/headline.title [node]
  [(keyword (str "h" (inc (- (org/prop :level node) @cv/level))))])

(defmethod element ::org-mode/link [node] [:a {:href (org/prop :path node)}])

(defmethod element ::org-mode/link.fuzzy [node]
  [:a {:href (org/prop :path node)}])

(defmethod element ::org-mode/link.https [node]
  [:a {:href (org/prop :raw-link node)}])

(defmethod element ::org-mode/link.file [s]
  [hiccup (-> s (org/with-type
                  (keyword (namespace ::file/_)
                           (->> s (org/prop :path) u/file-extension))))])

(doall (for [k u/image-file-types
             :let [qk (keyword (namespace ::file/_) k)]]
         (derive qk ::file/image)))

(defmethod element ::file/image [s]
  [defer/placeholder
   [:div [:img {:src (org/prop :path s)
                :width "50%"}]]])

(defmethod element ::file/pdf [[_ {:keys [path]}]]
  [defer/placeholder
   (into [:div]
         (for [n (rest (range 4))]
           [:f> pdf/pdf-canvas {:url         path
                                :page-number n}]))])

(derive ::org-mode/link.https ::org-mode/link.http)

(defmethod element ::org-mode/link.http [s]
  [:a {:href (org/prop :raw-link s)}])

(defmethod element ::org-mode/link.fuzzy [[_ {:keys [raw-link]}]]
  [:a {:href raw-link}])

(derive ::org-mode/link.gitlab ::org-mode/link.git)
(derive ::org-mode/link.github ::org-mode/link.git)

(defmethod element ::org-mode/link.git [[_ {:keys [path type]}]]
  [prune [:a {:style {:display "float"}
              :href (str "https://gitlab.com/" path)}
          [:span {:style {:background-color "#444"
                          :color "#ccc"
                          :padding "0 .4rem 0 .4rem"
                          :border-radius ".4rem 0 0 .4rem"}}
           (str "⇅ " type)]
          [:span {:style {:background-color "#ccc"
                          :padding "0 .4rem 0 .25rem"
                          :border-radius "0 .4rem .4rem 0"}} path]]])

(derive ::org-mode/link.embed:https ::org-mode/link.embed)

(defmethod element ::org-mode/link.embed [[_ {:keys [path]}]]
  [:iframe {:src path
            :loading "lazy"
            :style {:width "100%" :height 600}}])

(defmethod element ::org-mode/link.youtube [[_ {:keys [path]}]]
  [:iframe {:src (str "https://www.youtube.com/embed/" path)
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/link.vimeo [[_ {:keys [path]}]]
  [:iframe {:src (str "https://player.vimeo.com/video/"
                      path
                      "?title=0&byline=0&portrait=0&app_id=122963")
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/link.soundcloud [[_ {:keys [path]}]]
  [:iframe {:src (str "https://w.soundcloud.com/player/"
                      "?url=https%3A//api.soundcloud.com/playlists/"
                      path
                      "&color=%23ff5500&auto_play=false&hide_related=false"
                      "&show_comments=true&show_user=true&show_reposts=false"
                      "&show_teaser=true&visual=true")
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/link.soundcloudtrack [[_ {:keys [path]}]]
  [:iframe {:src (str "https://w.soundcloud.com/player/"
                      "?url=https%3A//api.soundcloud.com/tracks/"
                      path
                      "&color=%23ff5500&auto_play=false&hide_related=false"
                      "&show_comments=true&show_user=true&show_reposts=false"
                      "&show_teaser=true&visual=true")
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/plain-list [_] [:ul])

(defmethod element ::org-mode/plain-list.ordered [_] [:ol])

(defmethod element ::org-mode/plain-list.unordered [_] [:ul])

(defmethod element ::org-mode/plain-list.descriptive [_] [:ul])

(defmethod element ::org-mode/item [_]
  [:div {:style {:display "flex" :margin ".5rem 0 .5rem"}}])

(defmethod element ::org-mode/item.description [_]
  [:p])

(defmethod element ::org-mode/item.bullet [node]
  [:label {:style {:margin-right ".25rem"}} (org/prop :bullet node)])

(defmethod element ::org-mode/item.separator [_]
  [:label {:style {:margin "0 .15rem 0 .15rem"}} "◦"])

(defmethod element ::org-mode/table.org [_]
  [:div {:style {:width "50%"}}])

(defmethod element ::org-mode/table-row.standard [_]
  [:div {:style {:display "flex"
                 :justify-content "space-around"}}])

(defmethod element ::org-mode/table-cell [[_ {:keys [value]}]]
  [:div (str value)])

(defmethod element ::org-mode/span [_] [:span])

(defmethod element ::org-mode/src-block [[_ {:keys [value]}]]
  [:pre {:style {:max-width "50%"
                 :background "white"}} value])

(defmethod element ::org-mode/quote-block [_ {:keys [value]}]
  [:div {:style {:text-align    "justify"
                 :background "white"
                 :max-width "50%"
                 :margin-bottom "2rem"}}
   value])

(defmethod element ::org-mode/special-block.citation [[_ {:keys [parameters]}]]
  (let [m (clojure.edn/read-string
           (str "{" parameters "}"))]
    [:div {:style {:text-align    "justify"
                   :background "white"
                   :max-width "50%"
                   :margin-bottom "2rem"}}
     [:strong {:style {:margin-left "1rem"}}
      "- " (:author m)]]))
