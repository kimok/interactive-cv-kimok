(ns interactive-cv.org-edn
  (:require [clojure.zip :as z]
            [gnu.org-mode :as-alias org-mode]
            [interactive-cv.component :as-alias c]
            [interactive-cv :as-alias cv]
            [interactive-cv.element :as-alias el]
            [interactive-cv.zip :as cv.zip]
            #?(:cljs [json-html.core :refer [edn->hiccup]])))

(defn branch? [node]
  (and (vector? node)
       (-> node second map?)))

(def children (comp rest rest))

(defn props [node]
  (cond-> node
    (branch? node) second))

(defn make-node [node children]
  (into (subvec node 0 2) children))

(defn node-type [node]
  ((if (branch? node) first type) node))

(defn with-type [node k]
  (if-not (branch? node)
    node
    (make-node [k (props node)] (children node))))

(defn with-subtype [node k]
  (let [t (node-type node)]
    (with-type node (keyword (namespace t)
                             (str (name t) "." (name k))))))

(defn prop [k node]
  (some-> node props k))

(defn tag [node]
  (first (prop :tags node)))

(defn route [node]
  (when (isa? (node-type node) ::org-mode/headline)
    (tag node)))

(def ->zipper
  (partial z/zipper branch? (comp seq children) make-node))

(defn zip-walk [loc f & args]
  (->zipper (apply cv.zip/walk loc f args)))

(def dispatch node-type)

(defmulti element dispatch)

(def component element)

(defn hiccup [node]
  (if (string? node)
    node
    (let [debug #(cond->> %
                   (:dbg (meta %))
                   (conj [:<>
                          [:div {:style {:color "red"}}
                           [:strong (str (dispatch node))]
                           #?(:cljs [edn->hiccup node])]]))
          walk #(some->> node
                         children
                         (remove route)
                         (map hiccup)
                         (into %))]
      ((comp walk debug element) node))))

(defn prune [v & _] v)

(defmethod element :default [_] ^:dbg [:<>])

(defmethod element nil [_] [:<>])
