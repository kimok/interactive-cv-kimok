(ns interactive-cv.core
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require
   [interactive-cv.pdf :as pdf]
   [interactive-cv.zip :as cv.zip]
   [interactive-cv.org-edn :as org]
   [interactive-cv.route :as route :refer [route]]
   [cljs.core.async :refer [<!]]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [clojure.zip :as z]
   [cljs-http.client :as http]
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [reagent.ratom :refer [reaction]]
   [goog.object :as gobj]))

(def content (r/atom nil))

(def zipper
  (reaction
   (some-> (org/->zipper @content)
           (cv.zip/seek
            (cond->> org/route @route (comp #{(route/unslug @route)}))))))

(def level
  (reaction
   (some->> @zipper z/node (org/prop :level))))

(def root-route
  (reaction
   (some-> @zipper
           cv.zip/top
           (cv.zip/seek org/route)
           z/node
           org/route)))

(def child-routes (reaction (some->> @zipper z/children (filter org/route))))

(def parent-route (reaction (some-> @zipper z/up z/node)))

(def org-header
  (reaction (let [nodes (some-> (org/->zipper @content)
                                (cv.zip/seek #(#{'top-comment} (org/prop :mode %)))
                                z/up
                                z/children)]
              (into {}
                    (map #(do [(keyword (str/lower-case (:key %))) (:value %)]))
                    (map org/props nodes)))))

(when-let [^js pdfjs (gobj/get js/window "pdfjs-dist/build/pdf")]
  (set! (.. pdfjs -GlobalWorkerOptions -workerSrc)
        "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.8.335/pdf.worker.min.js")
  (pdf/init))

(defn fetch-content [& [{:keys [on-response]}]]
  (go (let [response (<! (http/get "content.edn"))
            body (:body response)]
        (reset! content (cond-> body (string? body) edn/read-string))
        (when on-response (on-response))
        (when-not @route (route/go! @root-route)))))

(defn mount [component]
  (when-let [el (.getElementById js/document "app")]
    (rdom/render [component] el)))
