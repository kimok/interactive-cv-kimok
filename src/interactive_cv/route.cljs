(ns interactive-cv.route
  (:require
   [clojure.string :as str]
   [reagent.core :as r]
   [reitit.core :as reitit]
   [reitit.frontend.easy :as easy]))

(defonce route (r/atom nil))

(defn slug [s] (some-> s
                       (str/replace #"_" "-")))

(defn unslug [s] (some-> s
                         (str/replace #"-" "_")))

(defn go! [path]
  (reset! route (slug path))
  (easy/push-state ::route {:name @route}))

(def router
  (reitit/router
   [["/:name" ::route]]))

(easy/start!
 router
 (fn [{{:keys [name]} :path-params}]
   (reset! route name))
 {:use-fragment false})
