(ns interactive-cv.util
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [clojure.zip :as z]))

(defn nth-of [coll v]
  (let [i (count (take-while #(not= v %) coll))]
    (when (or (< i (count coll))
              (= v (last coll)))
      i)))

(defn shift
  "Re-orders a sequence such that items after the index appear
  at the beggining.

  (translate 3 [1 2 3 4 5]) ; => [4 5 1 2 3]"
  [index coll]
  (if-not index
    coll
    (flatten (reverse (split-at index coll)))))

(defn partition-at
  "Like partition-by but will start a new run when f returns true.
  http://cninja.blogspot.com/2011/02/clojure-partition-at.html"
  [f coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (let [run (cons (first s) (take-while #(not (f %)) (rest s)))]
       (cons run (partition-at f (drop (count run) s)))))))

(defn hypotenuse [a b]
  (Math/sqrt (+ (* a a) (* b b))))

(defn file-extension [path] (let [parts (str/split path ".")]
                              (when (> (count parts) 1) (last parts))))

(def image-file-types [:jpg :png :jpeg :JPG :JPEG])

(def vectorize (partial walk/postwalk #(cond-> % (seq? %) vec)))

