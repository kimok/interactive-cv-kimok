(ns interactive-cv.parse
  (:require [clojure.string :as str]
            [clojure.zip :as z]
            [gnu.org-mode :as-alias org-mode]
            [interactive-cv.component :as-alias c]
            [interactive-cv.element :as-alias el]
            [interactive-cv.org-edn :as org]
            [interactive-cv.zip :as cv.zip]))

(defmulti infer-node (comp org/node-type z/node))

(defmethod infer-node :default [loc] loc)

(defmethod infer-node 'item [loc]
  (let [{:keys [tag bullet]} (org/props (z/node loc))]
    (cond-> loc
      (sequential? tag)
      (-> (z/edit update 1 dissoc :tag)
          (z/insert-child [::org-mode/item.separator {}])
          (z/insert-child [::org-mode/item.description {}])
          z/down
          ((partial reduce z/insert-child) (reverse tag))
          z/up)
      :then (z/insert-child [::org-mode/item.bullet
                             {:bullet bullet}]))))

(defmethod infer-node 'headline [loc]
  (-> loc
      (z/insert-child [::org-mode/headline.title
                       (-> loc z/node org/props)])
      z/down
      ((partial reduce z/insert-child)
       (->> loc z/node (org/prop :title) reverse))))

(defmethod infer-node 'link [loc]
  (cond-> loc
    (not (z/children loc))
    (z/insert-child [::org-mode/text
                     {:value
                      (->> loc z/node (org/prop :raw-link))}])))

(defmethod infer-node nil [loc]
  (z/remove loc))

(defn insert-post-blank [z]
  (let [pb (some->> z z/node (org/prop :post-blank))]
    (cond-> z
      (and (z/branch? z)
           (not (cv.zip/root? z))
           (some-> pb pos?))
      (z/insert-right
       [::org-mode/text {:value (str/join (repeat pb " "))}]))))

(defn element->component [node]
  (let [t (when (isa? (org/node-type node) ::org-mode/headline)
            (org/prop :todo-keyword node))]
    (cond-> node t (org/with-type (keyword (namespace ::c/_) (name t))))))

(defmulti infer-type #(if (org/branch? %) ::org/branch (type %)))

(defmethod infer-type (type "") [node]
  [::org-mode/text {:value node}])

(defmethod infer-type :default [node] node)

(defmethod infer-type ::org/branch [node] node
  (update node 0 #(keyword (namespace ::org-mode/_)
                           (name %))))

(defn infer-subtype [node]
  (if-let [t (org/prop :type node)]
    (org/with-subtype node t)
    node))
