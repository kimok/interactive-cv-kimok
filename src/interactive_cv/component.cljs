(ns interactive-cv.component
  (:require [interactive-cv.org-edn :as org :refer [component prune hiccup]]
            [interactive-cv.doc :as doc :refer [docs]]
            [org.gnu.org-mode :as-alias org-mode]))

(defmethod component ::EXAMPLE [_]
  [:div {:style {:display    "inline-block"
                 :padding "2rem"
                 :background (str "linear-gradient("
                                  "rgb(128, 255, 0), "
                                  "rgb(0, 255, 255))")}}])

(defmethod component ::EXAMPLE-CHILDREN [node]
  [prune
   (into [:div {:style {:display    "inline-block"
                        :padding "2rem"
                        :background (str "linear-gradient("
                                         "rgb(0, 128, 255), "
                                         "rgb(0, 255, 255))")}}]
         (comp (drop 1)
               (map hiccup))
         (org/children node))])

(defmethod component ::TODO [_]
  [:div [:strong {:style {:position "absolute"
                          :margin-top "-0.5rem"
                          :color "orange"
                          :text-shadow "1.5px 1px 0px black"
                          :transform "rotate(15deg)"}} "TODO"]])

(doc/add ::EXAMPLE "Example component. Adds a colorful background.

Usage:
** EXAMPLE <title>
<contents>")

(defmethod component ::DOC [m]
  (let [n (->> m (org/prop :title) first)
        doc (filter (comp #{n} name key)
                    @docs)]
    [prune (into [:pre {:style {:background "white"
                                :max-width "50em"}}]
                 (map (fn [e] [:div
                               [:h4 (str (key e))]
                               [:p (val e)]]))
                 doc)]))
