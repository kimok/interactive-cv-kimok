(ns interactive-cv.demo.core
  (:require [clojure.string :as str]
            [clojure.zip :as z]
            interactive-cv.element
            interactive-cv.component
            [interactive-cv.route :as route :refer [route]]
            [interactive-cv.core :as cv]
            [interactive-cv.org-edn :as org]
            [shadow.resource]))

(shadow.resource/inline "content/demo/content.org")

(def slug->str #(str/replace % #"_" " "))

(defn nav []
  (let [ts (map org/tag @cv/child-routes)]
    (when-not (empty? ts)
      (into [:div {:style {:display "flex"}} "↓"]
            (comp (map #(do [:a {:href %
                                 :style {:margin "0 0.5em 0 0.5em"}}
                             (slug->str %)]))
                  (interpose "|"))
            ts))))

(defn back []
  (when-let [t (org/tag @cv/parent-route)]
    [:span "↑ " [:a {:href t} (slug->str t)]]))

(defn app []
  [:<>
   [back]
   (some->> @cv/zipper z/node (conj [org/hiccup]))
   [:br]
   [nav]])

(defn init []
  (cv/fetch-content
   {:on-response #(when-not @route
                    (route/go! "interactive-cv"))})
  (cv/mount app))
